# 005_006_asyncio_treading_multiprocessing_GIL

# Поєднання двох лекцій:
# Асінхронне програмування у Python
# Багатопоточне програмування у Python

---

[__video 1__](https://youtu.be/cx1TWb2VrG0)

[__video 2__](https://youtu.be/3hVZ6j-XwvA)

### программа навчання: **Python Advanced 2022**

### номер заняття: 5, 6

### засоби навчання: Python; інтегроване середовище розробки (PyCharm або Microsoft Visual Studio + Python Tools for Visual Studio + можливість використання Юпітер Jupyter Notebook)

---

### Огляд, мета та призначення уроку



**Вивчивши матеріал даного заняття, учень зможе:**


**Зміст уроку:**
